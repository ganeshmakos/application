package com.makos.sendmoney.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.makos.sendmoney.InernetConnection.Connectivity;
import com.makos.sendmoney.R;
import com.makos.sendmoney.SingleTonRequest.AppController;
import com.makos.sendmoney.SingleTonRequest.CustomVolleyJsonRequest;
import com.makos.sendmoney.SingleTonRequest.MySingleton;
import com.makos.sendmoney.Util.SharedPreferencesUtils;
import com.makos.sendmoney.staticurl.StaticUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;

//https://www.journaldev.com/28028/android-q-location-permissions
//https://www.journaldev.com/13325/android-location-api-tracking-gps

public class LoginActivity extends AppCompatActivity {

    Button btn_login;
    Context mContext;
    TextView tv_register;
    EditText et_username,et_password;
    AlertDialog progressDialog;
    String emailid="no";
    public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = LoginActivity.this;

        btn_login = findViewById(R.id.btn_login);
        tv_register = findViewById(R.id.tv_register);
        et_username = findViewById(R.id.username_et);
        et_password = findViewById(R.id.password_et);
       //new SpotsDialog(mContext,R.style.Custom);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHome = new Intent(mContext, RegisterActivity.class);
                startActivity(intentHome);
            }
        });
    }

    public void validation()
    {
        if(et_username.getText().toString().equals("")) {
            Toast.makeText(mContext,"Enter Mobile Number",Toast.LENGTH_SHORT).show();
        }else /*if(isEmailValid(et_username.getText().toString())){
            emailid="yes";
        }else*/ if(et_password.getText().toString().equals(""))
        {
            Toast.makeText(mContext,"Enter Password",Toast.LENGTH_SHORT).show();
        }else {
            loginUser(et_username.getText().toString(),et_password.getText().toString());
        }
    }

    public void loginUser(String username, String password)
    {
        if(Connectivity.isConnected(mContext))
        {
            progressDialog.show();

            String targetUrl = String.format(StaticUrl.login,username,password,"4","6");
            Log.d("targetUrl",targetUrl);

           /* if(emailid.equals("no"))
            {
                targetUrl = String.format(StaticUrl.loginEmail,username,password);
                *//*targetUrl = StaticUrl.login +
                        "?email=%s" + Html.fromHtml(et_username.getText().toString())+
                        "&password%s=" + Html.fromHtml(et_password.getText().toString());*//*
            }else {
                targetUrl = String.format(StaticUrl.login,username,password,"4","6");
                *//*targetUrl = StaticUrl.login +
                        "?phone=" + Html.fromHtml(et_username.getText().toString()) +//"8316435180"
                        "&password%s=" + Html.fromHtml(et_password.getText().toString()) +//"Bik2006(k)"
                        "&lat=" + "4" +
                        "&long=" + "6";*//*
            }*/

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                   targetUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            Log.d("responseM",response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject statusJsonObject = jsonObject.getJSONObject("success");
                                int status = statusJsonObject.getInt("status");

                                if(status==200) {
                                  /*String user_id = statusJsonObject.getString("user_id");
                                    String token = statusJsonObject.getString("token");
                                    String first_name = statusJsonObject.getString("first_name");
                                    String last_name = statusJsonObject.getString("last_name");
                                    String username = statusJsonObject.getString("username");
                                    String email = statusJsonObject.getString("email");
                                    String phone = statusJsonObject.getString("phone");
                                    String available_balance = statusJsonObject.getString("available_balance");
                                    String session_id = statusJsonObject.getString("session_id");*/
                                    new SharedPreferencesUtils(mContext).setUserID(statusJsonObject.getString("user_id"));
                                    new SharedPreferencesUtils(mContext).setToken(statusJsonObject.getString("token"));
                                    new SharedPreferencesUtils(mContext).setFName(statusJsonObject.getString("first_name"));
                                    new SharedPreferencesUtils(mContext).setLName(statusJsonObject.getString("last_name"));
                                    new SharedPreferencesUtils(mContext).setUserName(statusJsonObject.getString("username"));
                                    new SharedPreferencesUtils(mContext).setEmailId(statusJsonObject.getString("email"));
                                    new SharedPreferencesUtils(mContext).setPhoneNumber(statusJsonObject.getString("phone"));
                                    new SharedPreferencesUtils(mContext).setAvailableBalance(statusJsonObject.getString("available_balance"));
                                    new SharedPreferencesUtils(mContext).setSessionId(statusJsonObject.getString("session_id"));
                                    new SharedPreferencesUtils(mContext).setLoginFlag(true);
                                    Intent intentHome = new Intent(mContext, HomeActivity.class);
                                    intentHome.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intentHome);
                                }
                                if(status==401)
                                {
                                    Toast.makeText(mContext,statusJsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                                }
                           } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
}
