package com.makos.sendmoney.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.makos.sendmoney.R;
import com.makos.sendmoney.SingleTonRequest.MySingleton;
import com.makos.sendmoney.staticurl.StaticUrl;

import org.json.JSONException;
import org.json.JSONObject;

import dmax.dialog.SpotsDialog;

public class RegisterActivity extends AppCompatActivity {

    Button btn_register;
    Context mContext;
    String MobilePattern = "[0-9]{10}";
    EditText et_firstname,et_lastname,et_password,et_repassword,et_mobile,et_email;
    public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
    AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = RegisterActivity.this;

        et_firstname = findViewById(R.id.firstname_et);
        et_email = findViewById(R.id.emailid_et);
        et_lastname = findViewById(R.id.lastname_et);
        et_password = findViewById(R.id.password_et);
        et_repassword = findViewById(R.id.password_confirm_et);
        et_mobile = findViewById(R.id.phone_et);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();


        btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validation()) {
              register(et_email.getText().toString(),et_firstname.getText().toString(),et_lastname.getText().toString(),et_password.getText().toString(),et_repassword.getText().toString(),et_mobile.getText().toString());

                }
            }
        });
    }

    private void register(String toString, String toString1, String toString2, String toString3, String toString4,String toString5) {

        progressDialog.show();

      String url=  String.format(StaticUrl.register, toString,toString1,toString2,toString3,toString4,toString5,1);

        Log.d("url",url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response",response);

                   progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject objectStatus  = jsonObject.getJSONObject("success");
                    String status = objectStatus.getString("status");
                    if(status.equals("200"))
                    {
                        Toast.makeText(mContext,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                        Intent intentHome = new Intent(mContext, LoginActivity.class);
                        startActivity(intentHome);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        MySingleton.getInstance(RegisterActivity.this).addToRequestQueue(request);
    }

    private boolean validation() {


       if(et_email.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter Email", Toast.LENGTH_SHORT).show();
           return false;
       }
       else if(!et_email.getText().toString().matches(EMAIL_VERIFICATION))
       {
           Toast.makeText(getApplicationContext(),"Enter Valid Email",Toast.LENGTH_SHORT).show();
           return false;
       }
       else if(et_firstname.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter First name", Toast.LENGTH_SHORT).show();
           return false;
       }
       else if(et_lastname.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter Last name", Toast.LENGTH_SHORT).show();
           return false;
       }

       else if(et_password.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_SHORT).show();
        return false;
       }
       else if(et_repassword.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter Confirm Password", Toast.LENGTH_SHORT).show();

       return  false;
       }
       else if(!et_repassword.getText().toString().equalsIgnoreCase(et_password.getText().toString()))
       {
           Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_SHORT).show();
          return false;
       }
       else if(et_mobile.getText().toString().isEmpty())
       {
           Toast.makeText(getApplicationContext(), "Enter Mobile", Toast.LENGTH_SHORT).show();
           return false;
       }
       else if(!et_mobile.getText().toString().matches(MobilePattern))
       {
           Toast.makeText(getApplicationContext(),"Enter Valid Mobile No",Toast.LENGTH_SHORT).show();
           return false;
       }

     return  true;
    }

}
