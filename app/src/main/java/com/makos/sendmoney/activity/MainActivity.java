package com.makos.sendmoney.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.makos.sendmoney.R;
import com.makos.sendmoney.Util.SharedPreferencesUtils;

//Splash screen
public class MainActivity extends AppCompatActivity {

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(new SharedPreferencesUtils(MainActivity.this).getLoginFlag()) {
                    Intent homeIntent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(homeIntent);
                    finish();
                }else {
                    Intent loginInent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(loginInent);
                    finish();
                }
            }
        },3000);

    }
}
