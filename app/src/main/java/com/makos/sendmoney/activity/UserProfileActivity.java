package com.makos.sendmoney.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.makos.sendmoney.R;
import com.makos.sendmoney.Util.SharedPreferencesUtils;

public class UserProfileActivity extends AppCompatActivity {


    TextView tv_firstname,tv_lastname,tv_phone,tv_email;
    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = UserProfileActivity.this;
        sharedPreferencesUtils = new  SharedPreferencesUtils(mContext);

        tv_firstname = findViewById(R.id.tv_firstname);
        tv_lastname = findViewById(R.id.tv_lastname);
        tv_phone = findViewById(R.id.tv_phone);
        tv_email = findViewById(R.id.tv_email);

        getProfileDetails();
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void getProfileDetails()
    {
        tv_firstname.setText(sharedPreferencesUtils.getFName());
        tv_lastname.setText(sharedPreferencesUtils.getLName());
        tv_phone.setText(sharedPreferencesUtils.getPhoneNumber());
        tv_email.setText(sharedPreferencesUtils.getEmailId());
    }

    /*http://test-env.zdqp4yckx8.us-east-2.elasticbeanstalk.com/api/get-user-specific-details?_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFjMTJlODZkZDY1YzYyYzhmOGQ5OGY1ZDk3ZTcwYzhhY2ZjMjk0OGU0YTAwZDFmMTc4YjA3MjdkNjM4YjUzZTQ3NzQzMGU5NGQ2Y2MxZTJjIn0.eyJhdWQiOiIzIiwianRpIjoiMWMxMmU4NmRkNjVjNjJjOGY4ZDk4ZjVkOTdlNzBjOGFjZmMyOTQ4ZTRhMDBkMWYxNzhiMDcyN2Q2MzhiNTNlNDc3NDMwZTk0ZDZjYzFlMmMiLCJpYXQiOjE1NTQ5NTAxNzAsIm5iZiI6MTU1NDk1MDE3MCwiZXhwIjoxNTg2NTcyNTcwLCJzdWIiOiIyNCIsInNjb3BlcyI6W119.FpYU4wzX8nVozmjAUw6CjCar0A5-ko7p1y-QIgGriX0uU2273SuXNbW0eaWigBluVUhi5EbjxYNUMSBBzUhDHMv9x1osuP2Po_4IdD1oTi6WIrcRWEkPcexJEDwBq4xxdmHyvptHk0rwQ_FTQ_W_WQguEtqNOmAk8GLlveMAOAGRQGoPOwV_R78RyolJFEAuEs6qX08G2XbPA701ogXcb--QTSRYDuFAd55F9GvV_5YqKOAgoriqpkMpxaxaQTNrRWQtALzQPp8YZIlB-VcdYLxGJK36G9YBjVf_lU6QZ1n6WCLqaU2tKglG8ZYgOqybdftnQXaN6k4s4uY3Bl6LYfRzpJsIrxevMmmQ-HueUbeGD8X7TRwvZhbSH0x_37hC3jVO31zDBSCjqR3RwLh4DPEmKkWzFZeVVBNuns5bxvpXI-63ovOD6733U_RIbj4sNxHtehdcis5yL-WWZmp0RiOUlVgL9UC5gwzBPJds5L5O1J34_wLB4LVYOnQ0hbGWfZHXJAgkw1I-_dwn8bdQRAb4L7B8gUgduRi35QYKS82l3N5Gyun4EN5_gIx8mxTevjyv8W5qe7F7YJGg9CpdVm7sU5VFKiZyYZQNGnWnLlqhcAt-kzfNwaXLldTZ8UrwcHt0RnzGG0gTSxP0lKDHu-vjJaljLp8BMUyPFYjVz1I &phone=8862025200*/

    /*
    * {
    "success": {
        "status": 200,
        "user": {
            "id": 24,
            "role_id": 2,
            "type": "user",
            "first_name": "Ganesh",
            "last_name": "Bhosale",
            "phone": "8862025200",
            "defaultCountry": "us",
            "carrierCode": "1",
            "email": "ganeshbhosale143@gmail.com",
            "address_verified": 0,
            "identity_verified": 0,
            "status": "Active",
            "created_at": "2019-04-11 07:10:16",
            "updated_at": "2019-04-11 07:10:16",
            "picture": "",
            "current_player_id": null
        }
    }
}*/
}
