package com.makos.sendmoney.Util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

    private Context _context;
    SharedPreferences prefs;

    public SharedPreferencesUtils(Context _context) {
        this._context = _context;
        try {
            prefs = this._context.getSharedPreferences("application", Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void logout()
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
    public void setLoginFlag(boolean isLogin) {

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("loginFlag", isLogin);
        editor.commit();
    }

    public boolean getLoginFlag() {
        if (prefs.contains("loginFlag"))
            return prefs.getBoolean("loginFlag", false);
        else
            return false;
    }

    public void setLoginusers(String loginusers)
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("loginusers", loginusers);
        editor.commit();
    }
    public String getLoginusers() {
        if (prefs.contains("loginusers"))
            return prefs.getString("loginusers", "");
        else
            return "";
    }

    //email id
    public void setEmailId(String EmailId) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("EmailId", EmailId);
        editor.commit();
    }
    public String getEmailId() {
        if (prefs.contains("EmailId"))
            return prefs.getString("EmailId", "");
        else
            return "";
    }
    //user id
    public void setUserId(String EmailId) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserId", EmailId);
        editor.commit();
    }
    public String geUserId() {
        if (prefs.contains("UserId"))
            return prefs.getString("UserId", "");
        else
            return "";
    }
    //token
    public int setToken(String Token) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Token", Token);
        editor.commit();
        return 0;
    }
    public String getToken() {
        if (prefs.contains("Token"))
            return prefs.getString("Token", "");
        else
            return "";
    }
    //first_name
    public int setFName(String Name) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("FName", Name);
        editor.commit();
        return 0;
    }
    public String getFName() {
        if (prefs.contains("FName"))
            return prefs.getString("FName", "");
        else
            return "";
    }
    //last_name
    public int setLName(String Name) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("LName", Name);
        editor.commit();
        return 0;
    }
    public String getLName() {
        if (prefs.contains("LName"))
            return prefs.getString("LName", "");
        else
            return "";
    }
    //username
    public int setUserName(String UserName) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserName", UserName);
        editor.commit();
        return 0;
    }
    public String getUserName() {
        if (prefs.contains("UserName"))
            return prefs.getString("UserName", "");
        else
            return "";
    }
    //phonenumber

    public int setPhoneNumber(String PhoneNumber) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("PhoneNumber", PhoneNumber);
        editor.commit();
        return 0;
    }
    public String getPhoneNumber() {
        if (prefs.contains("PhoneNumber"))
            return prefs.getString("PhoneNumber", "");
        else
            return "";
    }

    //available_balance
    public int setAvailableBalance(String LoginType) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("AvailableBalance", LoginType);
        editor.commit();
        return 0;
    }
    public String getAvailableBalance() {
        if (prefs.contains("AvailableBalance"))
            return prefs.getString("AvailableBalance", "");
        else
            return "";
    }
    //Sessionid
    public int setSessionId(String Sessionid) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Sessionid", Sessionid);
        editor.commit();
        return 0;
    }
    public String getSessionId() {
        if (prefs.contains("Sessionid"))
            return prefs.getString("Sessionid", "");
        else
            return "";
    }


    public void setuser_profile(String profile)
    {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Profile", profile);
        editor.commit();
    }
    public String get_Profile(){
        if (prefs.contains("Profile"))
        {
            return prefs.getString("Profile","");
        }
        else return "";
    }


    public int setImageUrl(String ImageUrl) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ImageUrl", ImageUrl);
        editor.commit();
        return 0;
    }
    public String getImageUrl() {
        if (prefs.contains("ImageUrl"))
            return prefs.getString("ImageUrl", "");
        else
            return "";
    }

    public int setMenuResponce(String MenuResponce) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("MenuResponce", MenuResponce);
        editor.commit();
        return 0;
    }
    public String getMenuResponce() {
        if (prefs.contains("MenuResponce"))
            return prefs.getString("MenuResponce", "");
        else
            return "";
    }

    public int setCatCode(String CatCode) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("CatCode", CatCode);
        editor.commit();
        return 0;
    }
    public String getCatCode() {
        if (prefs.contains("CatCode"))
            return prefs.getString("CatCode", "");
        else
            return "";
    }

    public int setRes_name(String Res_name) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Res_name", Res_name);
        editor.commit();
        return 0;
    }
    public String getRes_name() {
        if (prefs.contains("Res_name"))
            return prefs.getString("Res_name", "");
        else
            return "";
    }

    public int setOrder_code(String Order_code) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Order_code", Order_code);
        editor.commit();
        return 0;
    }
    public String getOrder_code() {
        if (prefs.contains("Order_code"))
            return prefs.getString("Order_code", "");
        else
            return "";
    }

    public int setPosition(int Position) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Position", String.valueOf(Position));
        editor.commit();
        return 0;
    }
    public String getPosition() {
        if (prefs.contains("Position"))
            return prefs.getString("Position", "");
        else
            return "";
    }

    public int setnewStatus(String newStatus) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("newStatus", newStatus);
        editor.commit();
        return 0;
    }
    public String getnewStatus() {
        if (prefs.contains("newStatus"))
            return prefs.getString("newStatus", "");
        else
            return "";
    }

    public int setUserID(String UserID) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("UserID", UserID);
        editor.commit();
        return 0;
    }
    public String getUserID() {
        if (prefs.contains("UserID"))
            return prefs.getString("UserID", "");
        else
            return "";
    }


}
